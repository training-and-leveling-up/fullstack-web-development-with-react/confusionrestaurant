# Ristorante Con Fusion

## Summary

This project is the result of the exercises proposed in the course _Front-End Web UI Frameworks and Tools: Bootstrap 4_, which belongs to the Coursera Specialized Program titled _Full-Stack Web Development with React_. This project builds the website of a restaurant using Bootstrap 4 components.

## Deployment

In order to start the development server, it is necessary to run the following command in the root folder of the project:

```
npm start
```

If it is the first time, please, run the following command to intall all dependencies, before starting the server:

```
npm install
```