$(document).ready(function() {
    $('#my_carousel').carousel({
        interval: 2000
    });
    $('#carousel_button').click(function() {
        if ($('#carousel_button').children('span').hasClass('fa-pause')) {
            $('#my_carousel').carousel('pause');
            $('#carousel_button').children('span').removeClass('fa-pause');
            $('#carousel_button').children('span').addClass('fa-play');
        } else if ($('#carousel_button').children('span').hasClass('fa-play')) {
            $('#my_carousel').carousel('cycle');
            $('#carousel_button').children('span').removeClass('fa-play');
            $('#carousel_button').children('span').addClass('fa-pause');
        }       
    });
    
    $('#reserve_table_modal').modal({
        keyboard: false,
        show: false
    });
    $('#reserve_table_button').click(function() {
        $('#reserve_table_modal').modal('show');
    });
    $('#cancel_reserve_modal').click(function() {
        $('#reserve_table_modal').modal('hide');
    });
    $('#close_reserve_modal').click(function() {
        $('#reserve_table_modal').modal('hide');
    })

    $('#login_modal').modal({
        keyboard: false,
        show: false
    });
    $('#login_button').click(function() {
        $('#login_modal').modal('show');
    });
    $('#cancel_login_modal').click(function() {
        $('#login_modal').modal('hide');
    });
    $('#close_login_modal').click(function() {
        $('#login_modal').modal('hide');
    })
});